<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class ProductController extends Controller
{
    public function index()
    {
        return Product::all();
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'desc' => 'required',
            'price' => 'required',
        ]);

        $product = new Product();
        $product->pname = $request->name;
        $product->pdesc = $request->desc;
        $product->price = $request->price;
        $product->save();
        return response('success');
    }

    public function show(Product $product)
    {
        return $product;
    }

    public function update(Request $request, Product $product)
    {
        $request->validate([
            'name' => 'required',
            'desc' => 'required',
            'price' => 'required',
        ]);
        $product= Product::find($product->id);
        $product->pname = $request->name;
        $product->pdesc = $request->desc;
        $product->price = $request->price;
        $product->save();
        return response('success');
    }

    public function destroy(Product $product)
    {
        $product->delete();
        return response('success');
    }
}
